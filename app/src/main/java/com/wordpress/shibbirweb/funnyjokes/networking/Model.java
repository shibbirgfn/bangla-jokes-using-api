package com.wordpress.shibbirweb.funnyjokes.networking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Model implements Serializable {

    @SerializedName("Category")
    @Expose
    private List<Category> category = null;
    @SerializedName("Collection")
    @Expose
    private List<Collection> collection = null;
    @SerializedName("app_data_date_time")
    @Expose
    private String appDataDateTime;
    @SerializedName("success")
    @Expose
    private Boolean success;
    private final static long serialVersionUID = 1951793844899400159L;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public List<Collection> getCollection() {
        return collection;
    }

    public void setCollection(List<Collection> collection) {
        this.collection = collection;
    }

    public String getAppDataDateTime() {
        return appDataDateTime;
    }

    public void setAppDataDateTime(String appDataDateTime) {
        this.appDataDateTime = appDataDateTime;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
