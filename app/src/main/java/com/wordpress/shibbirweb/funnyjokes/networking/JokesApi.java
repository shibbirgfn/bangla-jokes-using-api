package com.wordpress.shibbirweb.funnyjokes.networking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JokesApi {

    @GET("api/bangla_joks/index1.json")
    Call<Model>getJokes();
}
