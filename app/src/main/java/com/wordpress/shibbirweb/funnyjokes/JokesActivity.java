package com.wordpress.shibbirweb.funnyjokes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.wordpress.shibbirweb.funnyjokes.networking.Collection;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class JokesActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView categoryName, showHtml;
    private AVLoadingIndicatorView progressBar;
    private WebView webView;
    private Button backButton;

    private List<Collection> jokesCollections = new ArrayList<Collection>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jokes);

        categoryName = (TextView)findViewById(R.id.tv_activity_jokes_category_name);
        progressBar = (AVLoadingIndicatorView)findViewById(R.id.tv_activity_jokes_progress_bar);
        webView = (WebView)findViewById(R.id.wv_acitivity_jokes);
        backButton = (Button)findViewById(R.id.btn_acitivity_jokes_back_btn);
        showHtml = (TextView)findViewById(R.id.showHtml);

        if (getIntent().hasExtra("category_id") && getIntent().hasExtra("category_title")){

            int clickPosition = getIntent().getIntExtra("click_position",0);
            String  categoryID = getIntent().getStringExtra("category_id");
            String categoryTitle = getIntent().getStringExtra("category_title");
            categoryName.setText(String.valueOf(categoryTitle));

            //Toast.makeText(this, "cat "+categoryID, Toast.LENGTH_SHORT).show();

            jokesCollections = MainActivity.model.getCollection();




            //showHtml.setText(Html.fromHtml(MainActivity.model.getCollection().get(clickPosition).getDescr()));

            for (Collection jokes : jokesCollections){

                if (String.valueOf(jokes.getCategoryId()).equals(categoryID)){
                    stopAnim();
                    webView.loadData(jokes.getDescr(),"text/html;charset=utf-8","UTF-8");
                    break;
                }
            }








        }else{
            Toast.makeText(this, "No data available", Toast.LENGTH_SHORT).show();
            finish();
        }






        backButton.setOnClickListener(this);



    }

    void startAnim(){
        progressBar.show();
        // or avi.smoothToShow();
    }

    void stopAnim(){
        progressBar.hide();
        // or avi.smoothToHide();
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
