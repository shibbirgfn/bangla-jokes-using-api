package com.wordpress.shibbirweb.funnyjokes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.wordpress.shibbirweb.funnyjokes.networking.Category;

import java.util.ArrayList;
import java.util.List;
public class CustomCategoryAdapter extends ArrayAdapter {

    List<Category> categoryList = new ArrayList<Category>();

    public CustomCategoryAdapter(@NonNull Context context, List<Category> categoryList) {
        super(context, R.layout.item, categoryList);
        this.categoryList = categoryList;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item,parent,false);

        TextView jokesCategory = convertView.findViewById(R.id.item_btn_jokes_category);

        jokesCategory.setText(categoryList.get(position).getTitle());

        return convertView;
    }
}
