package com.wordpress.shibbirweb.funnyjokes;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.wordpress.shibbirweb.funnyjokes.networking.Category;
import com.wordpress.shibbirweb.funnyjokes.networking.JokesApi;
import com.wordpress.shibbirweb.funnyjokes.networking.Model;
import com.wordpress.shibbirweb.funnyjokes.networking.RetrofitClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private ListView categoryList;
    private AVLoadingIndicatorView progressBar;

    static Model model = new Model();
    List<Category> categories = new ArrayList<Category>();

    CustomCategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView)findViewById(R.id.textView);
        categoryList = (ListView)findViewById(R.id.lv_category_list);
        progressBar = (AVLoadingIndicatorView)findViewById(R.id.avi);

        JokesApi jokesApi = RetrofitClass.getRetrofit().create(JokesApi.class);

        Call<Model> modelCall = jokesApi.getJokes();

        showProgressBar();
        modelCall.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                model = null;
                model = response.body();

                categories.clear();
                categories = model.getCategory();

                categoryAdapter = new CustomCategoryAdapter(getApplicationContext(), categories);

                Collections.sort(categories, new Comparator<Category>() {


                    @Override
                    public int compare(Category o1, Category o2) {


                        return String.valueOf(o1.getTitle()).compareToIgnoreCase(String .valueOf(o2.getTitle()));
                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            return Integer.compare(Integer.parseInt(o1.getSortWeight()),Integer.parseInt(o2.getSortWeight()));
                        }else{
                            return Integer.compare(Integer.parseInt(o1.getSortWeight()),Integer.parseInt(o2.getSortWeight()));
                        }*/

                    }
                });

                hideProgressBar();
                categoryList.setAdapter(categoryAdapter);

                categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(MainActivity.this,JokesActivity.class);
                        intent.putExtra("click_position",position);
                        intent.putExtra("category_id",categories.get(position).getId());
                        intent.putExtra("category_title",categories.get(position).getTitle());
                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {

            }
        });


    }


    void showProgressBar(){
        progressBar.smoothToShow();
        // or avi.smoothToShow();
    }

    void hideProgressBar(){
        progressBar.hide();
        // or avi.smoothToHide();
    }
}
